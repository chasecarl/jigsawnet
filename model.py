import tensorflow as tf
from tensorflow import keras
from tensorflow.keras import layers
from keras.callbacks import ModelCheckpoint, CSVLogger
from tensorflow.keras.layers import TimeDistributed as td
from tensorflow.keras.layers import *


from os.path import isfile


from util import *


WEIGHTS_PATH = 'weights.h5'


def get_model():
    model = keras.Sequential([
        Input(shape=(12, 64, 64, 3)),

        Conv2D(50, kernel_size=(5,5), padding='same', activation='relu', strides=2),
        BatchNormalization(),
        td(MaxPooling2D()),

        td(Conv2D(100, kernel_size=(5,5), padding='same', activation='relu', strides=2)),
        td(BatchNormalization()),
        td(Dropout(0.3)),

        td(Conv2D(100, kernel_size=(3,3), padding='same', activation='relu', strides=2)),
        td(BatchNormalization()),
        td(Dropout(0.3)),

        td(Conv2D(200, kernel_size=(3,3), padding='same', activation='relu', strides=1)),
        td(BatchNormalization()),
        td(Dropout(0.3)),

        Flatten(),

        Dense(600, activation='relu'),
        BatchNormalization(),
        Dense(400, activation='relu'),
        BatchNormalization(),
        Dropout(0.3),
        Dense(144),
        Reshape((12, 12)),
        Activation('softmax'),
    ])
    if isfile(WEIGHTS_PATH):
        model.load_weights(WEIGHTS_PATH)
    else:
        print('No weights file was found. You need to train the model!')
    return model


if __name__ == "__main__":
    model = get_model()

    model_name = 'model_v5'
    model_path = f'{model_name}.h5'
    log_path = f'{model_name}.csv'
    checkpoint = ModelCheckpoint(model_path)
    logger = CSVLogger(log_path, append=True)
    callbacks = [checkpoint, logger]
    batch_size = 64
    epochs = 15
    debug = False
    model.compile(loss='categorical_crossentropy', optimizer='adam', metrics=['accuracy'], run_eagerly=debug)
    model.load_weights(model_path)

    train_IDs = get_filepaths('train')
    validation_IDs = get_filepaths('validation')
    train = TileDataGenerator(train_IDs, batch_size=batch_size)
    validation = TileDataGenerator(validation_IDs, batch_size=batch_size)

    model.fit(train, validation_data=validation, epochs=epochs, callbacks=callbacks)

