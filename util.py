import os
import numpy as np
from cv2 import imread, resize, IMREAD_COLOR
from sklearn.utils import shuffle
from matplotlib import pyplot as plt
from keras.utils import Sequence, to_categorical


# TARGET_SIZE = (400, 300)
SQUARE_SIDE = 64
TILE_SIZE = (SQUARE_SIDE, SQUARE_SIDE)
X, Y = 0, 1
SHUFFLE_AUGMENTATION_MULTIPLIER = 1
BOTTOM = 0
TOP = 1
LEFT = 2
RIGHT = 3
def get_x_y(img, sam=SHUFFLE_AUGMENTATION_MULTIPLIER, random_state=None):
    # cutting into tiless
    tiles = np.empty((12, *TILE_SIZE, 3))
    for row in range(1, 4):
        for col in range(1, 5):
            tiles[(row - 1) * 4 + col - 1, ] = img[TILE_SIZE[X] * (row - 1):TILE_SIZE[X] * row, TILE_SIZE[Y] * (col - 1):TILE_SIZE[Y] * col]
    # creating labels
    label = np.arange(12)
    # shuffling
    shuffled_xs, shuffled_ys = [], []  # the first has shape of (SHUFFLE_AUGMENTATION_MULTIPLIER, 12, *TILE_SIZE, 3)
    for i in range(sam):
        if random_state:
            shuffled_x, shuffled_y = shuffle(tiles, label, random_state=random_state)
        else:
            shuffled_x, shuffled_y = shuffle(tiles, label)
        shuffled_xs.append(shuffled_x)
        shuffled_ys.append(shuffled_y)
    if sam == 1:
        return shuffled_xs[0], shuffled_ys[0]
    return np.asarray(shuffled_xs), np.asarray(shuffled_ys)


def get_data(dir_path, random_state=0):
    prev_path = os.getcwd()
    os.chdir(dir_path)
    xs, ys = [], []
    for i, filename in enumerate(os.listdir(), start=1):
        img = imread(filename, IMREAD_COLOR)
        try:
            x, y = get_x_y(img, random_state=0) 
        except ValueError:
            print(f'Error while processing {filename}...')
            continue
        if x.ndim != 5:
            print(f'Image {filename} has ndim of {x.ndim} after preprocessing!')
            continue
        xs.append(x)
        ys.append(y)
    os.chdir(prev_path)
    return np.concatenate(xs), np.concatenate(ys)


def plot_sample(sample):
    fig = plt.figure()
    n_rows, n_cols = 3, 4
    for row in range(1, n_rows + 1):
        for col in range(1, n_cols + 1):
            ax = fig.add_subplot(n_rows, n_cols, (row - 1) * 4 + col)
            plt.imshow(sample[(row - 1) * 4 + col - 1].astype(np.uint8))
            plt.subplots_adjust(hspace=0, wspace=-0.38)
            plt.axis('off')


def get_filepaths(data_subfolder):
    prev_path = os.getcwd()
    os.chdir(f'data/{data_subfolder}')
    result = tuple(map(lambda filename: f'{data_subfolder}/{filename}', os.listdir()))
    os.chdir(prev_path)
    return result


def predict(model, sample):
    return model.predict(np.expand_dims(sample, 0))[0]


def from_pre_pred(pre_pred):
    while True:
        pred = np.argmax(pre_pred, axis=1)
        unq, unq_counts = np.unique(pred, return_counts=True)
        if unq.shape[0] == 12:
            break
        same_pred = unq[np.where(unq_counts > 1)]
        for tile_pred in same_pred:
            same_inds = np.where(pred == tile_pred)[0]
            pred_same_inds = pre_pred[same_inds]
            true_argmax_ind = same_inds[np.argmax(pred_same_inds) // 12]
            pre_pred[same_inds[same_inds != true_argmax_ind], tile_pred] = 0
    return pred


def from_pred(pred, sample):
    pred_sample = np.empty((12, 64, 64, 3))
    for i, tile_ind in enumerate(pred):
        pred_sample[tile_ind] = sample[i]
    return pred_sample


def solve(model, sample):
    pre_pred = predict(model, sample)
    pred = from_pre_pred(pre_pred)
    return from_pred(pred, sample)


within_tile_edge_thickness = 2
crop_amount = 64 - within_tile_edge_thickness
wtet = within_tile_edge_thickness  # alias


class TileDataGenerator(Sequence):

    WITHIN_TILE_EDGE_THICKNESS = wtet
    WTET = WITHIN_TILE_EDGE_THICKNESS
    CROP_AMOUNT = SQUARE_SIDE - WTET
    
    def __init__(self, list_IDs, batch_size=64, dim=(*TILE_SIZE, 3), n_tiles=12, shuffle=True, data_augmentation_multiplier=2, edges_mode=True):
        self.list_IDs = list_IDs
        self.batch_size = batch_size
        self.dim = dim
        self.n_tiles = n_tiles
        self.edges_mode = edges_mode
        if self.edges_mode:
            self.n_edges = 4 * self.n_tiles
        self.shuffle = shuffle
        self.data_augmentation_multiplier = data_augmentation_multiplier
        if not self.shuffle and self.data_augmentation_multiplier > 1:
            print('Warning: not shuffling the data while augmenting it!')
        self.on_epoch_end()
    
    
    def __len__(self):
        return int(np.floor(len(self.list_IDs) * self.data_augmentation_multiplier / self.batch_size))
    
    
    def __getitem__(self, index):
        indices = self.indices[index * self.batch_size:(index + 1) * self.batch_size]
        index_IDs = [self.list_IDs[k] for k in indices]
        X, y = self.__data_generation(index_IDs)
        return X, y
    

    def __edges_from_tiles(self, tiles):
        # tiles are of shape (n_tiles, *dim),
        # we want their edges of shape (n_edges, wtet, *dim[1:])
        edges = np.empty((self.n_edges, TileDataGenerator.WTET, *(self.dim[1:])))
        for i in range(len(tiles)):
            for j, cropping in enumerate((
                    (TileDataGenerator.CROP_AMOUNT, SQUARE_SIDE, 0, SQUARE_SIDE),  # bottom edge (None, crop_amount, 64, 3)
                    (0, TileDataGenerator.WTET, 0, SQUARE_SIDE),  # top edge  (None, crop_amount, 64, 3)
                    (0, SQUARE_SIDE, TileDataGenerator.CROP_AMOUNT, SQUARE_SIDE),  # right edge  (None, 64, crop_amount, 3)
                    (0, SQUARE_SIDE, 0, TileDataGenerator.WTET),  # left edge  (None, 64, crop_amount, 3)
                )):
                edge = tiles[i][cropping[BOTTOM]:cropping[TOP], cropping[LEFT]:cropping[RIGHT], :]
                if j > 1:
                    edge = np.transpose(edge, axes=[1, 0, 2])
                assert edge.shape == (2, 64, 3), f'shape is {edge.shape}, j is {j}'
                edges[i * 4 + j] = edge
        return edges


    def __all_pairs(self, x):
        # x is of shape (48, wtet, tile_size, 3)
        # we want (48 ^ 2 = 2304, 2 * wtet, tile_size, 3) array
        
        # I'm not actually sure which one of them is horizontal and which is vertical;
        # these are symbolic names that are designed to evoke associations with cartesian product though
        x_pre_horizontal = np.tile(np.expand_dims(x, 4), [1, 1, 1, 1, np.shape(x)[0]])
        x_horizontal = np.reshape(np.transpose(x_pre_horizontal, axes=[0, 4, 1, 2, 3]), (2304, wtet, 64, 3))
        x_pre_vertical = np.tile(np.expand_dims(x, 0), [np.shape(x)[0], 1, 1, 1, 1])
        x_vertical = np.reshape(x_pre_vertical, (2304, wtet, 64, 3))
    
        return np.concatenate([x_horizontal, x_vertical], axis=1)


    def __data_generation(self, index_IDs):
        if self.edges_mode:
            # (batch_size, 2304, 2 * wtet, 64, 3)
            X = np.empty((self.batch_size, self.n_edges ** 2, 2 * TileDataGenerator.WTET, *(self.dim[1:])))
        else:
            X = np.empty((self.batch_size, self.n_tiles, *self.dim))
        y = np.empty((self.batch_size, self.n_tiles), dtype=int)
        
        for i, ID in enumerate(index_IDs):
            filepath = f'data/{ID}'
            img = imread(filepath, IMREAD_COLOR)
            assert img is not None, filepath
            # TODO is there any reason to make the dam differ from 1?
            # TODO the augmentation happens only within the batch
            # X_i, y_i = get_x_y(img, sam=self.data_augmentation_multiplier)
            X_i, y_i = get_x_y(img, sam=1)
            if self.edges_mode:
                edges = self.__edges_from_tiles(X_i)  # (48, 2, 64, 3)
                pairs = self.__all_pairs(edges)  # (2304, 2 * wtet, 64, 3)
                assert pairs.shape == (2304, 2 * wtet, 64, 3), pairs.shape
                X_i = pairs
            X[i, ] = X_i
            y[i] = y_i
        
        return X, to_categorical(y, num_classes=self.n_tiles)
    
    
    def on_epoch_end(self):
        self.indices = np.tile(np.arange(len(self.list_IDs)), self.data_augmentation_multiplier)
        if self.shuffle:
            np.random.shuffle(self.indices)

