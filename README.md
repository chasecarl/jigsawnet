You first need to import the contents of `model.py` and `util.py`.  
To create a puzzle from an image, you first need to read the image into a numpy array.  
You should resize it so that its size is 256x192.  
Then, you can use the `get_x_y()` function that given an image cuts it in tiles, assigns labels, and shuffles it. It returns the shuffled array of shape (12, 64, 64, 3), i.e. 12 RGB tiles of size 64x64, also referred to as "sample", and corresponding labels (of shape (12, )). Now you have a puzzle ready to be solved! By the way, you can visualize a sample using `plot_sample()` function.  
Now, we need to load the solver itself. To do so, you need to use the `get_model()` function. You need to have the weights file in the same directory where you run the program. You can find them [here](https://drive.google.com/file/d/1LNUoZwFpLTY0Qzl_Qd8fnHM-MiGFswWV/view?usp=sharing). If you don't want to use our weights, you can train the model yourself by running the `model.py` script, though it takes a lot of time.  
Finally, to solve a puzzle you simply call the `solve()` function. It accepts a model and a sample, and returns a sample with the same tiles rearranged according to the order that the neural network predicted. As stated above, you can visualize the result using the same `plot_sample()` function.

